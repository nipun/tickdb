package org.nipunarora;

import java.util.*;

/**
 * This is the query processor which has all the compute logic
 */
public class QueryProcessor {

    TickDB tick_db;


    QueryProcessor(TickDB tick_db){
        this.tick_db = tick_db;
    }

    /**
     * Execute Print request for non-optimized version
     *
     * We first find the starting offset and then doa linear pass on all records where the symbol equals the query
     * @param q
     */
    public void executePrint(Query q){

        if(ProcessRequests.debug)
            System.out.println("Execute Print Query non-optimized");

        ArrayList<Tick> list = tick_db.list;
        Tick tempTick = new Tick();
        tempTick.setTs(q.getStart()-1);

        int i1 = Collections.binarySearch(list, tempTick);
        i1= i1==-1?0:i1;

        for(int i = i1; i<list.size(); i++){
            Tick t = list.get(i);

            if(ProcessRequests.debug)
                System.out.println("Looking at " + t);

            if(t.getTs()<q.getStart())continue;
            if(t.getTs()>q.getEnd()) break;

            if(t.getSymbol().equalsIgnoreCase(q.getSymbol())){
                System.out.println(t);
            }
        }
    }

    /**
     * Execute Product query non-optimized version
     *
     * We first find the starting offset and then do a linear pass of all records adding to the final sum value wherever it meets our requirements
     * @param q
     */
    public void executeProduct(Query q){

        if(ProcessRequests.debug)
            System.out.println("Execute Product Query non-optimized");

        ArrayList<Tick> list = tick_db.list;
        Tick tempTick = new Tick();
        tempTick.setTs(q.getStart()-1);

        int i1 = Collections.binarySearch(list,tempTick);
        i1= i1==-1?0:i1;

        double sum = 0l;

        for(int i = i1; i<list.size(); i++){
            Tick t = list.get(i);

            if(ProcessRequests.debug)
                System.out.println(" Looking at " + t);

            if(t.getTs()<q.getStart())continue;
            if(t.getTs()>q.getEnd()) break;

            if(t.getSymbol().equalsIgnoreCase(q.getSymbol()) && t.getFieldMap().containsKey(q.getField1()) && t.getFieldMap().containsKey(q.getField2()))
                sum+= t.getFieldMap().get(q.getField1())*t.getFieldMap().get(q.getField2());
        }

        System.out.println("Product is: " + sum);
    }

    /**
     * Execute Print Optimized version of the query
     *
     * We first find the starting offset in the column using a sorted index ( sorted map), and then iterate till the end.
     *
     * @param q
     */
    public void executePrintOptimized(Query q){

        if(ProcessRequests.debug)
            System.out.println("Executing Optimized Print Query");

        if(tick_db.symbolMapOffset.containsKey(q.getSymbol()) &&
                tick_db.symbolListMap.containsKey(q.getSymbol())){

            TreeMap<Long, Integer> treeMap = tick_db.symbolMapOffset.get(q.getSymbol());
            Map.Entry<Long, Integer> treeEntry = treeMap.ceilingEntry(q.getStart());
            ArrayList<Tick> ticks = tick_db.symbolListMap.get(q.getSymbol());

            for(int i = treeEntry.getValue(); i<ticks.size(); i++){
                Tick tick = ticks.get(i);

                if(ProcessRequests.debug)
                    System.out.println("Looking at " + tick);

                if(tick.getTs()<q.getStart()) break;
                if(tick.getTs()>q.getEnd()) break;

                System.out.println(tick);
            }
        }
    }

    /**
     * This function implements an algorithm similar to the standard sort-merge join.
     * We do a lockstep pass from the starting offsets for both column (field1, field2).
     * If both timestamps are equal we add the product of the values to the sum.
     * Else we move the smaller timestamp column forward
     * @param q
     */
    public void executeProductOptimized(Query q){

        if(ProcessRequests.debug)
            System.out.println("Executing Optimized Product Request");

        double sum = 0l;

        String field1Key = q.getSymbol()+"-"+q.getField1();
        String field2Key = q.getSymbol()+"-"+q.getField2();

        if(tick_db.fieldMapOffset.containsKey(field1Key) &&
                tick_db.fieldMapOffset.containsKey(field2Key) &&
                tick_db.fieldListMap.containsKey(field1Key) &&
                tick_db.fieldListMap.containsKey(field2Key)){

            int lo1 = tick_db.fieldMapOffset.get(field1Key).ceilingEntry(q.getStart()-1).getValue();
            int lo2 = tick_db.fieldMapOffset.get(field2Key).ceilingEntry(q.getStart()-1).getValue();

            ArrayList<Tick> field1List = tick_db.fieldListMap.get(field1Key);
            ArrayList<Tick> field2List = tick_db.fieldListMap.get(field2Key);

            while(lo1<field1List.size() && lo2<field2List.size()){
                Tick field1Tick = field1List.get(lo1);
                Tick field2Tick = field2List.get(lo2);

                if(ProcessRequests.debug)
                    System.out.println("Looking at " + field1Tick + " and " + field2Tick);


                if(field1Tick.getTs()<q.getStart()){ lo1++; continue; }
                if(field2Tick.getTs()<q.getStart()){ lo2++; continue; }

                if(field1Tick.getTs()>q.getEnd()|| field2Tick.getTs()>q.getEnd()) break;

                if(field1Tick.getTs()==field2Tick.getTs()){
                    if(field1Tick.getId()==field2Tick.getId()) {
                        //duplicates are managed here by adding to the sum only if we verify that it's the same record, referred to in both columns
                        sum += field1Tick.getFieldMap().get(q.getField1()) * field1Tick.getFieldMap().get(q.getField2());
                        lo1++;
                        lo2++;
                    }else{
                        //if timestamps are the same but records are different we move forward the one which is on the older record.
                        if(field1Tick.getId()<field2Tick.getId())
                            lo1++;
                        else
                            lo2++;
                    }
                }else if(field1Tick.getTs()<field2Tick.getTs()){
                    lo1++;
                }else{
                    lo2++;
                }
            }
        }
        System.out.println("The product is: " + sum);
    }

    public void executeQuery(Query q){
        try {
            if (q.getType() == Query.QueryType.print) {
                if (ProcessRequests.oPrint) executePrintOptimized(q);
                else executePrint(q);
            }
            if (q.getType() == Query.QueryType.product) {
                if (ProcessRequests.oProduct) executeProductOptimized(q);
                else executeProduct(q);
            }
        }catch (Exception e){
            System.out.println("Error in processing query");
            e.printStackTrace();
        }
    }

}
