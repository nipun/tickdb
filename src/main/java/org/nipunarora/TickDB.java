package org.nipunarora;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.*;


/**
 * This is our database class which mantains all records
 */
public class TickDB {

    /**
     * Main array list to store all tick objects used for non-optimized version
     */
    ArrayList<Tick> list = new ArrayList<Tick>();


    /* POPULATING DATABASES FOR PRINT OPTIMIZATION */

    /**
     * Column based store for each symbol
     */
    HashMap<String,ArrayList<Tick>> symbolListMap = new HashMap<String, ArrayList<Tick>>();

    /**
     * Column based sorted maps for each epoc timestamp and offset pairs.
     * Essentially given symbol A, we can figure out a TreeMap<Long,Integer>, where using the query starting timestamp, the starting offset can be gotten from the value
     * The offset can be used to get direct access to the symbolListMap.
     */
    HashMap<String,TreeMap<Long, Integer>> symbolMapOffset = new HashMap<String,TreeMap<Long, Integer>>();

    /**
     *
     * @param tick
     * @param counter
     */
    public void populateSymbolMapOffset(Tick tick,int counter){
        if(symbolMapOffset.containsKey(tick.getSymbol())){
            symbolMapOffset.get(tick.getSymbol()).put(tick.getTs(),counter);
        }else{
            TreeMap<Long,Integer> temp = new TreeMap<Long, Integer>();
            temp.put(tick.getTs(),counter);
            symbolMapOffset.put(tick.getSymbol(),temp);
        }
    }

    /**
     *
     * @param tick
     */
    public void populateSymbolListAndOffset(Tick tick){

        if(symbolListMap.containsKey(tick.getSymbol())){
            ArrayList<Tick> ticks = symbolListMap.get(tick.getSymbol());
            Tick tempTick = ticks.get(ticks.size() - 1);
            ticks.add(tick);//duplicate enteries are added here
            // IMPORTANT NOTE: update the starting offset of this symbol only if it's a new timestamp.
            // This takes care of duplicates in our TreeMap
            if(tempTick.getTs()!=tick.getTs())
                populateSymbolMapOffset(tick,(ticks.size()-1));
        }else{
            ArrayList<Tick> ticks = new ArrayList<Tick>();
            ticks.add(tick);
            symbolListMap.put(tick.getSymbol(),ticks);
            populateSymbolMapOffset(tick,0);
        }
    }


    /* POPULATING DATABASES FOR PRODUCT OPTIMIZATION */

    /**
     * Column oriented database for fields
     * Here Key is "symbol-fieldname" Hence for the following record Epoc: 1231724491 Signal ABC cost:0.5 id:512652.0
     * The keys will be "ABC-cost", "ABC-id" etc.
     */
    HashMap<String,ArrayList<Tick>> fieldListMap = new HashMap<String, ArrayList<Tick>>();

    /**
     * Key based SortedMap mapping using epoch timestamp as sorted map keys. This gives us starting offsets for each arraylist
     */
    HashMap<String,TreeMap<Long,Integer>> fieldMapOffset = new HashMap<String, TreeMap<Long, Integer>>();

    public void populateFieldMapOffset(String fieldKey, long ts, int counter){
        if(fieldMapOffset.containsKey(fieldKey)){
            fieldMapOffset.get(fieldKey).put(ts,counter);
        }else{
            TreeMap<Long,Integer> temp = new TreeMap<Long, Integer>();
            temp.put(ts,counter);
            fieldMapOffset.put(fieldKey,temp);
        }
    }

    public void populateFieldListAndOffset(Tick tick){
        Iterator<String> it = tick.getFieldMap().keySet().iterator();

        while (it.hasNext()){
            String fieldKey = tick.getSymbol() + "-" + it.next();

            if(fieldListMap.containsKey(fieldKey)){
                ArrayList<Tick> ticks = fieldListMap.get(fieldKey);
                Tick tempTick = ticks.get(ticks.size() - 1);
                ticks.add(tick);
                if(tempTick.getTs()!=tick.getTs())
                    populateFieldMapOffset(fieldKey,tick.getTs(),(ticks.size()-1));

            }else{
                ArrayList<Tick> ticks = new ArrayList<Tick>();
                ticks.add(tick);
                fieldListMap.put(fieldKey,ticks);
                populateFieldMapOffset(fieldKey,tick.getTs(),0);
            }
        }
    }



    /**
     * Populating to our data structures
     * @param tick
     */
    public void populateDB(Tick tick){

        list.add(tick);
        if(ProcessRequests.oPrint)
            populateSymbolListAndOffset(tick);
        if(ProcessRequests.oProduct)
            populateFieldListAndOffset(tick);
    }


    /**
     * Read Tick File
     * @param fileName file name of the tick file
     */
    public void readTickFile(String fileName){

        File f = new File(fileName);
        int counter = 0;
        try {
            Scanner sc = new Scanner(new FileInputStream(f));

            while(sc.hasNextLine()){
                String next = sc.next();
                Tick tick = new Tick();
                tick.parseTick(next,counter++);
                populateDB(tick);
                System.out.println(tick);
            }
        } catch (FileNotFoundException e) {
            System.out.println("Tick Parsing Failure ");
            e.printStackTrace();
        }

    }

}
