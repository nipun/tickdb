package org.nipunarora;

import org.kohsuke.args4j.Argument;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


/**
 * Main class which will process all requests
 */
public class ProcessRequests {


    public boolean isoPrint() {
        return oPrint;
    }

    public boolean isoProduct() {
        return oProduct;
    }

    @Option(name = "-Oprint", required = false, usage = "apply print optimization")
    public static boolean oPrint = false;

    @Option(name = "-Oproduct", required = false, usage = "apply product optimization")
    public static boolean oProduct = false;

    @Option(name = "-h", aliases = "--help", usage = "displays help message")
    boolean help = false;

    @Option(name = "-d", aliases = "--debug", usage = "prints debug output")
    static boolean debug = false;

    @Argument
    private List<String> arguments = new ArrayList<String>();

    String tickFile;

    public void doMain(String[] args){
        CmdLineParser parser = new CmdLineParser(this);
        try {

            parser.parseArgument(args);

            if(help) {
                parser.printUsage(System.out);
                System.exit(0);
            }
            if(arguments.isEmpty())
                throw new CmdLineException(parser,"No tick file given");
            else
                tickFile = arguments.get(0);

            if(arguments.size()!=1)
                throw new CmdLineException(parser,"Only give one tick file instance at a time");

        } catch (CmdLineException e) {
            e.printStackTrace();
            parser.printUsage(System.err);
        }

    }

    public void parseUserCommand(TickDB tdb){

        QueryProcessor qp = new QueryProcessor(tdb);
        Scanner sc = new Scanner(System.in);

        while (true){
            try {
                System.out.print(">");
                String command = sc.nextLine();
                Query q = new Query();
                q.parseQuery(command);
                qp.executeQuery(q);
                System.out.println("Finished Processing your Query \n");
            }catch (Exception e){
                System.out.println("Error in Processing last query");
                e.printStackTrace();
            }
        }

    }


    public static void main(String[] args){

        ProcessRequests pq = new ProcessRequests();
        pq.doMain(args);

        TickDB tb = new TickDB();
        tb.readTickFile(pq.tickFile);

        //start query command line
        pq.parseUserCommand(tb);
    }
}
