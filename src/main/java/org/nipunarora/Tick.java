package org.nipunarora;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;

/**
 * This class mantains all the records and a unique identifier for the line number of each record to manage duplicates
 */
public class Tick implements Comparable<Tick>{

    /**
     * The symbol of this record
     */
    private String symbol;

    /**
     * A hashmap of fields,values of the record
     */
    HashMap<String,Double> FieldMap = new HashMap<String, Double>();

    /**
     * A unique identifier for each record
     */
    private int id;

    /**
     * The timestamp for the record
     */
    private Long ts;

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public Long getTs() {
        return ts;
    }

    public void setTs(Long ts) {
        this.ts = ts;
    }


    public HashMap<String, Double> getFieldMap() {
        return FieldMap;
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void populate(String key, Double value){
        FieldMap.put(key,value);
    }


    public void parseTick(String next, int counter){

        Scanner sc2 = new Scanner(next);
        sc2.useDelimiter(",");

        if(sc2.hasNext()) this.setTs(Long.valueOf(sc2.next()));
        else System.err.println("Error TimeStamp Not Found!");

        if(sc2.hasNext()) this.setSymbol(sc2.next());
        else System.err.println("Error Symbol Not Found");

        while (sc2.hasNext()) {
            String key= sc2.next();
            String value = sc2.next();
            this.populate(key,Double.valueOf(value));
        }
        //record id to manage duplicates
        this.id = counter;
    }

    /**
     * Output formatting of the record for the print command
     * @return
     */
    public String toString(){
        StringBuffer sb = new StringBuffer("Epoc: " + ts + " Signal " + symbol + " ");
        Iterator<Map.Entry<String, Double>> it = FieldMap.entrySet().iterator();
        while (it.hasNext()){
            Map.Entry<String, Double> next = it.next();
            sb.append(next.getKey() + ":" + next.getValue() + " ");
        }
        return sb.toString();
    }

    /**
     * Used by comparator in TreeMaps
     * @param o
     * @return
     */
    @Override
    public int compareTo(Tick o) {

        if(this.getTs()==o.getId())
            return ((Integer)this.getId()).compareTo(o.getId());

        return this.getTs().compareTo(o.getTs());
    }

}
