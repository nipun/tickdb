package org.nipunarora;

import java.util.Scanner;

/**
 * Class that manages the query parsing
 */
public class Query {

    public enum QueryType{
        product,print
    };

    public QueryType getType() {
        return type;
    }

    public void setType(QueryType type) {
        this.type = type;
    }

    public Long getStart() {
        return start;
    }

    public void setStart(Long start) {
        this.start = start;
    }

    public Long getEnd() {
        return end;
    }

    public void setEnd(Long end) {
        this.end = end;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getField1() {
        return field1;
    }

    public void setField1(String field1) {
        this.field1 = field1;
    }

    public String getField2() {
        return field2;
    }

    public void setField2(String field2) {
        this.field2 = field2;
    }

    /**
     * Query type if it's product or print query
     */
    private QueryType type;

    /**
     * Start TimeStamp
     */
    private Long start;

    /**
     * End TimeStamp
     */
    private Long end;

    /**
     * Symbol
     */
    private String symbol;

    /**
     * Field1 for product query
     */
    private String field1;

    /**
     * Field2 for product query
     */
    private String field2;

    /**
     * Parse input query, with limited error checking
     * @param s
     */
    public void parseQuery(String s){
        try {
            Scanner sc = new Scanner(s);
            sc.useDelimiter(" ");

            String command = null;

            if (sc.hasNext())
                command = sc.next();
            else {
                System.out.println("Query command not recognized: " + command);
                return;
            }

            if (command.equalsIgnoreCase("print")) {
                this.type = QueryType.print;
            } else if (command.equalsIgnoreCase("product")) {
                this.type = QueryType.product;
            } else {
                System.out.println("Query command not recognized: " + command);
                return;
            }

            this.start = Long.valueOf(sc.next());
            this.end = Long.valueOf(sc.next());
            this.symbol = sc.next();

            if (this.type.equals(QueryType.product)) {
                this.field1 = sc.next();
                this.field2 = sc.next();
            }
        }catch (Exception e){
            System.err.println("Error in parsing query: " + s);
        }
    }
}
