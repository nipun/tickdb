

*SOLUTION*
--------

Overview of the classes in the solution

1. Tick Class - A Tick class file for representing each record, also has an id field as a unique identifier for each record.
2. TickDB Class - Mantains the data structures for keeping the tick file data
3. Query Class - Mantains query data structure
4. Query Processor - Contains compute logic for the queries, i.e. the query processing algorithm
5. Process Requests - The main class of the application


Requirements:
-----

    Java 7 JDK
    Maven 3.0


Build:
-----

    mvn package

Clean:
-----

    mvn clean


Usage:
----
    java -cp target/libs/args4j-2.33.jar -jar target/tickDB-1.0.jar -h

             -Oprint      : apply print optimization (default: false)
             -Oproduct    : apply product optimization (default: false)
             -d (--debug) : debug (default: false) (this flag is used to show how many enteries are looked at to get an idea of record pruning)
             -h (--help)  : help (default: true)


*Non optimized solution, brute force solution* :
-------
The basic solution is mantaining a simple arraylist which keeps all recorded tick objects. A binary search for the starting timestamp of the query gives the starting offset in this arraylist.
We then iterate through to the ending time stamp, both the print and product queries are executed for each tick record which matches the filters :- Symbol name for Print command, and Symbol Names + Field Names for product commands.

*Print Optimized* :
--------

Broadly the technique is similar to column oriented database index.
For print optimization, we mantain two data-structures, firstly we mantain an ArrayList<Ticks> for each symbol observed. This is kept in the following data structure:

        HashMap<String,ArrayList<Tick>> symbolListMap = new HashMap<String, ArrayList<Tick>>();

The above datastructure has seperate contiguous lists, for each symbol. To find the starting offset for the symbol given in the query. We use another datastructure as follows:

        HashMap<String,TreeMap<Long, Integer>> symbolMapOffset = new HashMap<String,TreeMap<Long, Integer>>();

Here the starting offset of each timestamp is kept in a sorted treemap based on timestamps. We find the CeilingEntry (the least timestamp greater than or equal to the starting timestamp in the query), and then do a linear pass from the corresponding offset to do a print.


*Product Optimized*:
-------

Broadly the technique is similar to column oriented database index, and a *sort-merge join*. For product optimization we use similar data-structures as the print optimization, in addition we do a lockstep on sorted array lists. The data is kept in the following data structure:

        HashMap<String,ArrayList<Tick>> fieldListMap = new HashMap<String, ArrayList<Tick>>();

In the above data structure we mantain a mapping of an ArrayList of Tick objects for each composite key "symbol-fieldName" (we assume that symbol-fieldname will be unique). Hence for the following record Epoc: 1231724491 Signal ABC cost:0.5 id:512652.0. The keys will be "ABC-cost", "ABC-id" etc.

        HashMap<String,TreeMap<Long,Integer>> fieldMapOffset = new HashMap<String, TreeMap<Long, Integer>>();

Same as the print optimization, we also keep a sorted treemap of timestamps to find the starting offset for the left and right column for both field1 and field2 in the product query. We do a lockstep pass from the starting offsets for both column (field1, field2). If both timestamps are equal we add the product of the values to the sum. Else we move the smaller timestamp column forward. This process is called a lockstep

Since duplicates could be a problem in this optimization, we use an "id" field in the tick as a record identifier, the sum is only added if the id is the same otherwise if timestamps is the same and id's are different we do a lockstep iteration on id's.

*NOTE*
-----
1. Unlike C/C++, ArrayLists in Java are a list of object references and do not offer contiguous memory allocation guarantees. For the current implementation I have kept any optimization for contiguous allocations/deep-copying of each column outside of the scope of the implementation.

2. To manage duplicates records are sorted first on timestamps, and then on record id. This ensures we have the first offset always.

*Example Results demostrating pruning*
--------
* Print Optimization Example

The debug flag shows how many records were looked at when running a certain query. In the non-optimal version of the query below, 8 enteries are looked at.

        ~/w/P/tickDB ❯❯❯ java -cp target/libs/args4j-2.33.jar -jar target/tickDB-1.0.jar data -d

        Epoc: 1231724491 Signal ABC cost:0.5 id:512652.0
        Epoc: 1231724491 Signal DEF cost:0.5 id:512653.0
        Epoc: 1231724492 Signal DEF img:1.0 cost:0.5 id:512654.0
        Epoc: 1231724493 Signal DEF cost:0.5 id:512654.0
        Epoc: 1231724494 Signal DEF img:2.0 cost:2.0 id:512654.0
        Epoc: 1231724494 Signal DEF img:3.0 cost:2.0 id:512652.0
        Epoc: 1231724496 Signal ABC cost:0.5 id:512654.0
        Epoc: 1231724497 Signal DEF cost:0.5 id:512654.0
        >print 1231724491 1231724497 ABC
        Execute Print Query non-optimized
        Looking at Epoc: 1231724491 Signal ABC cost:0.5 id:512652.0
        Epoc: 1231724491 Signal ABC cost:0.5 id:512652.0
        Looking at Epoc: 1231724491 Signal DEF cost:0.5 id:512653.0
        Looking at Epoc: 1231724492 Signal DEF img:1.0 cost:0.5 id:512654.0
        Looking at Epoc: 1231724493 Signal DEF cost:0.5 id:512654.0
        Looking at Epoc: 1231724494 Signal DEF img:2.0 cost:2.0 id:512654.0
        Looking at Epoc: 1231724494 Signal DEF img:3.0 cost:2.0 id:512652.0
        Looking at Epoc: 1231724496 Signal ABC cost:0.5 id:512654.0
        Epoc: 1231724496 Signal ABC cost:0.5 id:512654.0
        Looking at Epoc: 1231724497 Signal DEF cost:0.5 id:512654.0
        Finished Processing your Query

In the optimized version:

        ~/w/P/tickDB ❯❯❯ java -cp target/libs/args4j-2.33.jar -jar target/tickDB-1.0.jar data -Oprint -d

        Epoc: 1231724491 Signal ABC cost:0.5 id:512652.0
        Epoc: 1231724491 Signal DEF cost:0.5 id:512653.0
        Epoc: 1231724492 Signal DEF img:1.0 cost:0.5 id:512654.0
        Epoc: 1231724493 Signal DEF cost:0.5 id:512654.0
        Epoc: 1231724494 Signal DEF img:2.0 cost:2.0 id:512654.0
        Epoc: 1231724494 Signal DEF img:3.0 cost:2.0 id:512652.0
        Epoc: 1231724496 Signal ABC cost:0.5 id:512654.0
        Epoc: 1231724497 Signal DEF cost:0.5 id:512654.0
        >print 1231724491 1231724497 ABC
        Executing Optimized Print Query
        Looking at Epoc: 1231724491 Signal ABC cost:0.5 id:512652.0
        Epoc: 1231724491 Signal ABC cost:0.5 id:512652.0
        Looking at Epoc: 1231724496 Signal ABC cost:0.5 id:512654.0
        Epoc: 1231724496 Signal ABC cost:0.5 id:512654.0
        Finished Processing your Query

Only two records are looked at


* Product Optimization Example

Similarly in the product optimization we looked at 8 records without optimization

        ~/w/P/tickDB ❯❯❯ java -cp target/libs/args4j-2.33.jar -jar target/tickDB-1.0.jar data -d

        Epoc: 1231724491 Signal ABC cost:0.5 id:512652.0
        Epoc: 1231724491 Signal DEF cost:0.5 id:512653.0
        Epoc: 1231724492 Signal DEF img:1.0 cost:0.5 id:512654.0
        Epoc: 1231724493 Signal DEF cost:0.5 id:512654.0
        Epoc: 1231724494 Signal DEF img:2.0 cost:2.0 id:512654.0
        Epoc: 1231724494 Signal DEF img:3.0 cost:2.0 id:512652.0
        Epoc: 1231724496 Signal ABC cost:0.5 id:512654.0
        Epoc: 1231724497 Signal DEF cost:0.5 id:512654.0
        >product 1231724491 1231724497 ABC cost id
        Execute Product Query non-optimized
         Looking at Epoc: 1231724491 Signal ABC cost:0.5 id:512652.0
         Looking at Epoc: 1231724491 Signal DEF cost:0.5 id:512653.0
         Looking at Epoc: 1231724492 Signal DEF img:1.0 cost:0.5 id:512654.0
         Looking at Epoc: 1231724493 Signal DEF cost:0.5 id:512654.0
         Looking at Epoc: 1231724494 Signal DEF img:2.0 cost:2.0 id:512654.0
         Looking at Epoc: 1231724494 Signal DEF img:3.0 cost:2.0 id:512652.0
         Looking at Epoc: 1231724496 Signal ABC cost:0.5 id:512654.0
         Looking at Epoc: 1231724497 Signal DEF cost:0.5 id:512654.0
        Product is: 512653.0
        Finished Processing your Query

In the optimized version we only look at a small subset of records

        ~/w/P/tickDB ❯❯❯ java -cp target/libs/args4j-2.33.jar -jar target/tickDB-1.0.jar data -d -Oproduct

        Epoc: 1231724491 Signal ABC cost:0.5 id:512652.0
        Epoc: 1231724491 Signal DEF cost:0.5 id:512653.0
        Epoc: 1231724492 Signal DEF img:1.0 cost:0.5 id:512654.0
        Epoc: 1231724493 Signal DEF cost:0.5 id:512654.0
        Epoc: 1231724494 Signal DEF img:2.0 cost:2.0 id:512654.0
        Epoc: 1231724494 Signal DEF img:3.0 cost:2.0 id:512652.0
        Epoc: 1231724496 Signal ABC cost:0.5 id:512654.0
        Epoc: 1231724497 Signal DEF cost:0.5 id:512654.0
        >product 1231724491 1231724497 ABC cost id
        Executing Optimized Product Request
        Looking at Epoc: 1231724491 Signal ABC cost:0.5 id:512652.0  and Epoc: 1231724491 Signal ABC cost:0.5 id:512652.0
        Looking at Epoc: 1231724496 Signal ABC cost:0.5 id:512654.0  and Epoc: 1231724496 Signal ABC cost:0.5 id:512654.0
        The product is: 512653.0
        Finished Processing your Query