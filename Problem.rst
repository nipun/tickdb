

README
========

**Data**:

We are given a tick file as follows:

    <timestamp>,<symbol>,<field name 1>, <field value 1>, <field name 2>, <field value 2>, …

Here:

    <timestamp> is an integer UNIX time_t timestamp representing seconds since the EPOC.
    <symbol>,<field name 1>,<field name 2>…  are strings
    <field value 1>,<field value 2>,… are a double precision floating point values.


Example Tick File:

    319528800,Symbol1,Field1,10.0,Field2,20.0
    319528801,Symbol2,Field2,30.0
    319528801,Symbol2,Field2,50.0,Field3,60.0,Field4,20.0,Field1,100.0
    319528802,Symbol3,Field5,10.0,Field1,0.5

TickFile is guaranteed to be in ascending timestamp order. However duplicate timestamps may be present


The problem description is divided into support fro two different kind of queries.

1. Print Query

    Query:
        print <start time> <end time> <symbol>

    Function:

        For all ticks between <start time> and <end time> which have symbol <symbol>, print <field>:<value> for each <field> present in the tick.


2.  Product Query

    Query:

        product <start time> <end time> <symbol> <field1> <field2>

    Function:

        Print the sum of <field1> x <field2> for all ticks between <start time> and <end time> which have entries for both <field1> and <field2>. Ticks that only have entries for one of the fields should be ignored


Optional Optimizations:

    -Oprint
        Optimize the program for a single tickfile command, 10,000 print commands and 100 product commands.

    -Oproduct
        Optimize the program for a single tickfile command, 10,000 product commands and 100 print commands.
